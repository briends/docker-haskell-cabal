# haskell-cabal
[![Docker Repository on Quay.io](https://quay.io/repository/briends/haskell-cabal/status "Docker Repository on Quay.io")](https://quay.io/repository/briends/haskell-cabal)

base build image to build cabalized/stack haskell projects.

## Usage

Put this in a `Dockerfile` into your project root, next to your cabal file:

```
FROM quay.io/briends/haskell-cabal:7.8-onbuild
```

or for a `stack` based build image

```
FROM quay.io/briends/haskell-cabal:lts-2.19-onbuild
```

Configure project flags and other settings in the mandatory `cabal.config` file. Optionally: freeze your dependencies with `cabal freeze`.

Add a `.dockerignore` file like this:

```
.git
.cabal-sandbox
cabal.sandbox.config
dist
.stack-work
```
