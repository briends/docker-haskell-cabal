REPO = quay.io/briends/haskell-cabal
VERSION = 7.8
LTS_VERSION = lts-2.20


default: all

all: | onbuild

## Building
#
onbuild: | cabal-onbuild stack-onbuild

cabal-onbuild: onbuild/Dockerfile
	@docker build -t $(REPO):$(VERSION)-onbuild onbuild

stack-onbuild: stack/onbuild/Dockerfile
	@docker build -t $(REPO):${LTS_VERSION}-onbuild stack/onbuild

## Fill Templates
#
stack/onbuild/Dockerfile: stack/onbuild/Dockerfile.template
	@sed -e "s/{{LTS_VERSION}}/$(LTS_VERSION)/g" stack/onbuild/Dockerfile.template > stack/onbuild/Dockerfile

## Publishing
#
publish: | publish-stack publish-cabal

publish-stack: | stack-onbuild
	@docker push $(REPO):$(VERSION)-onbuild

publish-cabal: | cabal-onbuild
	@docker push $(REPO):${LTS_VERSION}-onbuild

## Maintenance
#
clean:
	@ rm stack/onbuild/Dockerfile

.PHONY: default onbuild cabal-onbuild stack-onbuild clean
